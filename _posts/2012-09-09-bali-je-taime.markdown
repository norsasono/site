---
layout: post
category: blog
title: Bali Je T'Aime
date: '2012-09-09 05:00:00'
---

Seperti yang sudah pernah saya katakan sebelumnya, September adalah bulan yang  cukup menyenangkan, semoga ..

{% image imgfull 2012/09/kuta.jpg %}
{% image imgfull 2012/09/people.jpg %}
{% image imgfull 2012/09/bedugul.jpg %}
{% image imgfull 2012/09/bedugul-2.jpg %}

Terimakasih telah mengijinkanku tersesat di pulau ini meskipun hanya dalam beberapa hari tanpa ada rencana apapun sebelumnya, *but it's okay*, saya cukup menikmatinya.
