---
layout:     project
category:   project

title:      Carpet
headline:   Super simple Sass structure
date:       2016-09-26
preview:    /images/2016/09/carpet.png

direct_url: /carpet
---