---
layout: page
title: About
---

Kutipan yang menginspirasi akan saya beri tempat sendiri. Karena cara termudah untuk mengingat sesuatu adalah dengan menulisnya.

***

<center>***</center>


> "Aywa pegat ngudia ronging budyayu, margane suka basuki, dimen luwar kang kinayun, kalis ing panggawe sisip, ingkang taberi prihatos." **—Raden Ngabehi Ronggowarsito III**

<center>***</center>

> "One’s own culture provides the “lens” through which  we view the world; the “logic” by which  we order it; the “grammar” by which it makes sense." **—Dennis J.D. Sandole and Hugo van der Merwe**

<center>***</center>

> "Seperti garam yang selalu memainkan perannya diam-diam. Tidak perlu berwujud, ia hanya perlu larut." **—Ana Monica Rufisa**

<center>***</center>

> "Siapa yang hidup untuk orang lain, akan hidup kelelahan. Akan tetapi ia akan hidup sebagai orang besar dan mati sebagai orang besar." <div class="cite">**—Anonim**</div>

<center>***</center>

> "The world is a big place, let's start to make it smaller" <div class="cite">**—Captain America**</div>

<center>***</center>

> "Learning another language is not only learning different words for the same things, but learning another way to think about things." <div class="cite">**—Flora Lewis**</div>

<center>***</center>

> "It’s very easy to hold on to hate. It’s very difficult to live that way." <div class="cite">**—Daniel Eden**</div>

<center>***</center>

> "The present is really a part of the past — a recent past — delusively given as being a time that s intervenes between the past and the future. Let it be named the specious present, and let the past, that is given as being the past, be known as the obvious past." <div class="cite">**—William James**</div>

<center>***</center>

> "Everything not saved will be lost." <div class="cite">**—Nintendo ‘quit screen’ message**</div>

<center>***</center>

> When I’m old and dying, I plan to look back on my life and say “wow, that was an adventure,” not “wow, I sure felt safe.”  <div class="cite">**—Tom Preston-Werner**</div>

<center>***</center>

> "To infinity .. and beyond .. !!"  <div class="cite">**—Buzz Lightyear**</div>

<center>***</center>

*to be continued ...*
