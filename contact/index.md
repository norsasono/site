---
layout: page
title: About
---

<figure>
    <img src="http://sasono.web.id/assets/images/profile-bw-2.jpg" class="imgbleed">
    <figcaption>In Catania, Sicily. Photo by George Kekatos.</figcaption>
</figure>

<!-- {% image imgfull profile-bw-2.jpg %} -->
Halo, nama saya **Noreta Agus Sasono**. I also speak [English](http://sasono.web.id).

Saya merupakan bagian dari [Nobayuke](http://fb.com/nobayuke); sebuah tempat kreatif yang terbentuk pada tahun 2011 untuk memenuhi kebutuhan orang atas desain dan fotografi yang ekslusif serta personal. Klik [disini](http://instagram.com/nobayuke) untuk melihat beberapa hal yang telah kami buat.

Jika kalian penasaran dengan apa blog ini berjalan, saya sudah menyediakan halaman [Colophon](/colophon) atau silahkan baca postingan [ini](/2015/02/25/hello-world/).

Oh ya, kalian juga bisa membaca beberapa [kutipan](/quotes) favorit yang sengaja saya tulis ulang di blog ini. Salam ..

I am currently living in Ngawi, Indonesia. I do design and photography for Nobayuke; a creative team established in 2011 to satisfy the needs of people of an exclusive design and photography. Click here to see our works and things we've made.


***

Facebook: [16 September](http://fb.com/norsasono)<br>
Twitter : [@norsasono](http://twitter.com/norsasono)<br>
Instagram : [Noreta Agus Sasono](http://instagram.com/norsasono)<br>
Email : noreta(@)sasono.web.id
