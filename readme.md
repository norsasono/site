### Welcome
This is [my website](http://sasono.web.id). Styled with [Sass](http://sass-lang.com), generated in [Jekyll](http://jekyllrb.com). Nothing fancy about it.

I decided to open source it for those who are interested in that kind of thing. Who knows? My code can hopefully help you learn something new.