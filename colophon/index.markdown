---
layout: page
title: Colophon
date: '2016-05-05 14:42:29'
---

Halaman ini saya sediakan untuk kalian yang mungkin *kepo* tentang informasi tambahan mengenai blog ini.

Blog ini sebenarnya hanyalah halaman statis yang berjalan menggunakan :octopus: [Jekyll](http://jekyll.org), itulah salah satu alasan mengapa blog ini relatif lebih cepat kalian buka sekalipun dengan koneksi yang lemot. Semua file saya tempatkan online pada :octocat: [Github Pages](http://pages.github.com). Blog ini menggunakan tema :cactus: [Kactus](https://github.com/nickbalestra/kactus) dengan beberapa perubahan. 

Ada pertanyaan? silahkan tanyakan langsung melalui kolom komentar atau dengan mengirimkan email ke **noreta(at)sasono.web.id**
